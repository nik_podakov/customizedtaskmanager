﻿using System;
using System.Diagnostics;
using System.Linq;
using ThreadState = System.Diagnostics.ThreadState;

namespace CustomizedTaskManager
{
	public class ProcessDetails
	{
		private readonly PerformanceCounter _cpuUsageCounter;
		private readonly PerformanceCounter _memoryCounter;
		private readonly PerformanceCounter _workingSetCounter;
		public string Name { get; set; }
		public int Id { get; set; }
		public string Status { get; set; }
		public string CpuPercent => Math.Round(Cpu, 1) + "%";
		public float Cpu { get; set; }
		private double Memory { get; set; }
		private double WorkingSet { get; set; }
		public string MemoryString => Memory < 512 ? Memory + "KB" : Math.Round(Memory / 1024.0, 1) + "MB";
		public string WorkingSetString => WorkingSet < 512 ? WorkingSet + "KB" : Math.Round(WorkingSet / 1024.0, 1) + "MB";
		public Process Process { get; private set; }

		public ProcessDetails(Process process)
		{
			try
			{
				Process = process;
				Name = process.ProcessName;
				if (Name == "dllhost" || Name == "SynTPEnh")
				{
					return;
				}
				Id = process.Id;
				Status =
				(from object thread in process.Threads select thread as ProcessThread).Any(
					thread =>
						thread.ThreadState != ThreadState.Wait ||
						(thread.ThreadState == ThreadState.Wait && thread.WaitReason != ThreadWaitReason.Suspended))
					? "Running"
					: "Suspended";
				_cpuUsageCounter = new PerformanceCounter("Process", "% Processor Time", process.ProcessName);
				_cpuUsageCounter.NextValue();
				_memoryCounter = new PerformanceCounter("Process", "Working Set", process.ProcessName);
				_workingSetCounter = new PerformanceCounter("Process", "Working Set - Private", process.ProcessName);
				WorkingSet = Math.Round(_memoryCounter.NextValue()/1024.0/1024.0, 1);
				Memory = Math.Round(_workingSetCounter.NextValue()/1024.0/1024.0, 1);
				Cpu = _cpuUsageCounter.NextValue();
				Cpu = Cpu < 100 ? Cpu : 0;
				
			}
			catch (Exception)
			{
				//ignored
			}
		}

		public void Update()
		{
			try
			{
				WorkingSet = Math.Round(_memoryCounter.NextValue() / 1024.0 / 1024.0, 1);
				Memory = Math.Round(_workingSetCounter.NextValue() / 1024.0 / 1024.0, 1);
				Cpu = _cpuUsageCounter.NextValue();
				Cpu = Cpu < 100 ? Cpu : 0;
			}
			catch (Exception)
			{
				// ignored
			}
		}
	}
}