﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Win32;

namespace CustomizedTaskManager
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		public ObservableCollection<ProcessDetails> Processes { get; set; }
		public ObservableCollection<KeyValuePair<string, string>> StartupProcesses { get; set; }
		public ObservableCollection<ServiceController> Services { get; set; }
		private readonly PerformanceCounter _cpuUsageCounter;
		private readonly PerformanceCounter _memoryUsageCounter;
		public int TotalMemory { get; }
		public ObservableCollection<KeyValuePair<long, double>> CpuUsage { get; set; }
		private long _time;
		public ObservableCollection<KeyValuePair<long, double>> MemoryUsage { get; set; }

		private GridViewColumnHeader _processesListViewSortCol;
		private SortAdorner _processesListViewSortAdorner;

		public MainWindow()
		{
			Processes = new ObservableCollection<ProcessDetails>();
			StartupProcesses = new ObservableCollection<KeyValuePair<string, string>>();
			Services = new ObservableCollection<ServiceController>();
			CpuUsage = new ObservableCollection<KeyValuePair<long, double>>();
			MemoryUsage = new ObservableCollection<KeyValuePair<long, double>>();
			_cpuUsageCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
			_cpuUsageCounter.NextValue();
			_memoryUsageCounter = new PerformanceCounter("Memory", "Available MBytes", true);
			TotalMemory = (int) Math.Round((new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory) / 1024.0 / 1024.0, 0);
			for (var i = 0; i < 60; i++)
			{
				CpuUsage.Add(new KeyValuePair<long, double>(_time, 0));
				MemoryUsage.Add(new KeyValuePair<long, double>(_time, 0));
				_time++;
			}
			UpdateServices();
			FillStartupProcesses();
			InitializeComponent();
			UpdateProcesses();
			UpdateResourceUsages();
			var timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 1)};
			timer.Tick += Timer_Tick;
			timer.IsEnabled = true;
			//new Thread(UpdateAll).Start();
		}

		void Timer_Tick(object sender, EventArgs e)
		{
			UpdateResourceUsages();
		}

		private void UpdateResourceUsages()
		{
			var currentCpuUsage = Math.Round(_cpuUsageCounter.NextValue(), 2);
			var currentMemoryUsage = TotalMemory - _memoryUsageCounter.NextValue();
			CpuUsage.RemoveAt(0);
			CpuUsage.Add(new KeyValuePair<long, double>(_time, currentCpuUsage));
			MemoryUsage.RemoveAt(0);
			MemoryUsage.Add(new KeyValuePair<long, double>(_time, currentMemoryUsage));
			CpuUsageTextBlock.Text = "CPU usage: " + currentCpuUsage + "%";
			MemoryUsageTextBlock.Text = "Memory usage: " + currentMemoryUsage + "MB / " + TotalMemory + "MB";
			_time++;
		}

		private void UpdateServices()
		{
			foreach (var serviceController in ServiceController.GetServices())
			{
				Services.Add(serviceController);
			}
		}

		private void UpdateProcesses()
		{
			Processes.Clear();
			foreach (var process in Process.GetProcesses().Select(x => new ProcessDetails(x)))
			{
				Processes.Add(process);
			}
		}

		private void FillStartupProcesses()
		{
			const string runKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
			using (var startupKey = Registry.LocalMachine.OpenSubKey(runKey))
			{
				if (startupKey == null) return;
				foreach (
					var program in
						startupKey.GetValueNames()
							.Where(valueName => startupKey.GetValueKind(valueName) == RegistryValueKind.String)
							.ToDictionary(valueName => valueName, valueName => startupKey.GetValue(valueName).ToString()))
				{
					StartupProcesses.Add(program);
				}
			}
			using (var startupKey = Registry.CurrentUser.OpenSubKey(runKey))
			{
				if (startupKey == null) return;
				foreach (
					var program in
						startupKey.GetValueNames()
							.Where(valueName => startupKey.GetValueKind(valueName) == RegistryValueKind.String)
							.ToDictionary(valueName => valueName, valueName => startupKey.GetValue(valueName).ToString()))
				{
					StartupProcesses.Add(program);
				}
			}
		}

		private void ProcessesListViewHeaderClick(object sender, RoutedEventArgs e)
		{
			var column = sender as GridViewColumnHeader;
			if (column == null) return;
			var sortBy = column.Tag.ToString();
			if (_processesListViewSortCol != null)
			{
				AdornerLayer.GetAdornerLayer(_processesListViewSortCol).Remove(_processesListViewSortAdorner);
				ProcessesListView.Items.SortDescriptions.Clear();
			}

			var newDir = ListSortDirection.Ascending;
			if (Equals(_processesListViewSortCol, column) && _processesListViewSortAdorner.Direction == newDir)
				newDir = ListSortDirection.Descending;

			_processesListViewSortCol = column;
			_processesListViewSortAdorner = new SortAdorner(_processesListViewSortCol, newDir);
			AdornerLayer.GetAdornerLayer(_processesListViewSortCol).Add(_processesListViewSortAdorner);
			ProcessesListView.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
		}

		private void RefreshMenuItem_OnClick(object sender, RoutedEventArgs e)
		{
			UpdateProcesses();
		}
	}
	public class SortAdorner : Adorner
	{
		private static readonly Geometry AscGeometry =
				Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

		private static readonly Geometry DescGeometry =
				Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

		public ListSortDirection Direction { get; }

		public SortAdorner(UIElement element, ListSortDirection dir)
				: base(element)
		{
			Direction = dir;
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			base.OnRender(drawingContext);

			if (AdornedElement.RenderSize.Width < 20)
				return;

			var transform = new TranslateTransform
					(
							AdornedElement.RenderSize.Width - 15,
							(AdornedElement.RenderSize.Height - 5) / 2
					);
			drawingContext.PushTransform(transform);

			var geometry = AscGeometry;
			if (Direction == ListSortDirection.Descending)
				geometry = DescGeometry;
			drawingContext.DrawGeometry(Brushes.Black, null, geometry);

			drawingContext.Pop();
		}
	}
}
